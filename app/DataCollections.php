<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DataCollections extends Model
{
    //
      protected $table="dataCollections";
      protected $fillable = [
				'salutations',
                'firstName',
                'middleName',
                'lastName',
                'address',
                'skype',
                'email',
                'phone',
                'employment_status',
                'position',
                'startdate',
                'salaray_range',
                'resume',
                'skills',
                'personal_data_reserve1',
                'personal_data_reserve2',
                'personal_data_reserve3',
                'personal_data_reserve4',
                'personal_data_reserve5',
                'wfh_capability_1',
                'wfh_capability_2',
                'wfh_capability_3',
                'wfh_capability_4',
                'wfh_capability_5',
                'wfh_capability_6',
                'wfh_capability_7',
                'wfh_capability_8',
                'wfh_capability_9',
                'wfh_capability_10',
                'wfh_capability_11',
                'wfh_capability_12',
                'wfh_capability_13',
                'wfh_capability_14',
                'wfh_capability_15',
    ];
      protected $casts = [
       'skills' => 'array',
       'personal_data_reserve1' => 'array'
      
    ];
}
