<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\DataCollections;
use Validator;

class DataCollectionsController extends Controller
{
    //
    public function datacollections(Request $request)
    {

    	$Validator = Validator::make($request->all(),[

					'firstName' => 'required',
					'lastName' => 'required',
					'address' => 'required',
					'skype' => 'required',
					'email' => 'required',
					'phone' => 'required|numeric',
					'employment_status' => 'required',
					'position' => 'required',
					'resume' => 'required|mimes:pdf,doc,docx',
					'skills' => 'required'
    	]);

    	  	if ($Validator->fails()) {
    	  		return response()->json($Validator->messages(), 200);

	            // return response()->json(['error'=>$Validator->errors()], 422);   
                //testing mic
	        }

		 $fileName = null;
	   if (request()->hasFile('resume')) {
	        $file = request()->file('resume');
	        $fileName = md5($file->getClientOriginalName() . time()) . "." . $file->getClientOriginalExtension();
	        $file->move('./documents/', $fileName);    
	    }

	     $datacollections = new DataCollections([
			'salutations'=> $request->get('salutations'),
			'firstName'=> $request->get('firstName'),
			'middleName'=> $request->get('middleName'),
			'lastName'=> $request->get('lastName'),
			'address'=> $request->get('address'),
			'skype'=> $request->get('skype'),
			'email'=> $request->get('email'),
			'phone'=> $request->get('phone'),
			'employment_status'=> $request->get('employment_status'),
			'position'=> $request->get('position'),
			'startdate'=> $request->get('startdate'),
			'salaray_range'=> $request->get('salaray_range'),
			'resume'=> $fileName,
			'skills'=> $request->get('skills'),
		    'personal_data_reserve1'=> $request->get('personal_data_reserve1'),
            'personal_data_reserve2'=> $request->get('personal_data_reserve2'),
            'personal_data_reserve3'=> $request->get('personal_data_reserve3'),
            'personal_data_reserve4'=> $request->get('personal_data_reserve4'),
            'personal_data_reserve5'=> $request->get('personal_data_reserve5'),
            'wfh_capability_1'=> $request->get('wfh_capability_1'),
            'wfh_capability_2'=> $request->get('wfh_capability_2'),
            'wfh_capability_3'=> $request->get('wfh_capability_3'),
            'wfh_capability_4'=> $request->get('wfh_capability_4'),
            'wfh_capability_5'=> $request->get('wfh_capability_5'),
            'wfh_capability_6'=> $request->get('wfh_capability_6'),
            'wfh_capability_7'=> $request->get('wfh_capability_7'),
            'wfh_capability_8'=> $request->get('wfh_capability_8'),
            'wfh_capability_9'=> $request->get('wfh_capability_9'),
            'wfh_capability_10'=> $request->get('wfh_capability_10'),
            'wfh_capability_11'=> $request->get('wfh_capability_11'),
            'wfh_capability_12'=> $request->get('wfh_capability_12'),
            'wfh_capability_13'=> $request->get('wfh_capability_13'),
            'wfh_capability_14'=> $request->get('wfh_capability_14'),
            'wfh_capability_15'=> $request->get('wfh_capability_15'),

		]);

		$datacollections->save();

    	return response()->json($datacollections);



    }

    public function dataQueryadmin () {

    	$positions = DataCollections::select('position')->groupBy('position')->selectRaw('count(*) as total, position')->get();

      $alldatacount =  DataCollections::count();
    	return response()->json(array("position"=>$positions, "alldatacount"=>$alldatacount));

    }


    public function mainSkills() {

    	$mainSkills = DataCollections::select('personal_data_reserve1')->get();

    	  	foreach ($mainSkills as $key => $mainSkill) {
   					    $results[] = $mainSkill->personal_data_reserve1;
					    
   			}
   			 $arrayToString = implode(",",$results);
   			 $stringToArray = explode(",",$arrayToString);
   			 $countData = array_count_values($stringToArray);
   			 $arrayUnique = array_unique($stringToArray);


    	return response()->json($countData);
    }


    public function mainSkillsBreakDown(Request $request)
    {


            $inputdata = $request->get('skills');

            $mainSkills = preg_split('/\s+/', $inputdata, -1, PREG_SPLIT_NO_EMPTY); 

            $users = DataCollections::where(function ($q) use ($mainSkills) {
              foreach ($mainSkills as $value) {
                $q->orWhere('personal_data_reserve1', 'like', "%{$value}%");
              }
            })->get();


    	return response()->json(array("data"=>$users));
    }


    
    public function dataSkillsQUery() {
    	$skills = DataCollections::select('skills')->groupBy('skills')->get();
   			foreach ($skills as $key => $skill) {
   					    $results[] = $skill->skills;
					    
   			}
   			 $arrayToString = implode(",",$results);
   			 $stringToArray = explode(",",$arrayToString);

    	return response()->json($stringToArray);
    }

    public function postionQueryData(Request $request){

    	 $dataposition = $request->get('positionQuery');
    	 $position = DataCollections::where("position", $dataposition)->get();
    	return response()->json(array("data"=>$position));
    	
    }

    public function queryApplicantsBy($id)
    {

        $summarydata = DataCollections::where('id',$id)->first();

        return response()->json($summarydata);
    }

    public function allapplication()
    {

      $allapplication = DataCollections::Orderby('created_at','desc')->get();

      return response()->json(array("data"=>$allapplication));
    }


}
