<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
use Validator;

class UserController extends Controller
{
	    //

	     public $successStatus = 200;


	    /**
	     * login api testestse
	     *
	     * @return \Illuminate\Http\Response d
	     */
	    public function login(){
	        if(Auth::attempt(['email' => request('email'), 'password' => request('password')])){
	            $user = Auth::user();
	            $success['token'] =  $user->createToken('MyApp')->accessToken;
	            return response()->json(['success' => $success], $this->successStatus);
	        }
	        else{
	            return response("error")->json(['error'=>'Unauthorised'], 401);
	        }
	    }


	    /**
	     * Register api
	     *
	     * @return \Illuminate\Http\Response
	     */
	    public function register(Request $request)
	    {
	        $validator = Validator::make($request->all(), [
	            'name' => 'required',
	            'email' => 'required|email',
	            'password' => 'required',
	            'c_password' => 'required|same:password',
	        ]);


	        if ($validator->fails()) {
	            return response()->json(['error'=>$validator->errors()], 401);            
	        }


	        $input = $request->all();
	        $input['password'] = bcrypt($input['password']);
	        $user = User::create($input);
	        $success['token'] =  $user->createToken('MyApp')->accessToken;
	        $success['name'] =  $user->name;
	        $success['email'] =  $user->email;


	        return response()->json(['success'=>$success], $this->successStatus);
	    }
}
