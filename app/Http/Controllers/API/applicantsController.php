<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\Applicant;
use Validator;
class applicantsController extends Controller
{
    //

    public function applicationForm(Request $request)
    {

    	$validator = Validator::make($request->all(),[
			 'firstName' => 'required',
		     'lastName' => 'required',
		     'address' => 'required',
		     'email' => 'required',
		     'phone' => 'required|numeric',
		     'position' => 'required',
		     'resume' => 'required|mimes:pdf,doc,docx',
		     'startdate' => 'date',
		     'skill1' => 'required',
		     'skill1_experience' => 'required|numeric',
		     'skill2' => 'required',
		     'skill2_experience' => 'required|numeric',
		     'skill3' => 'required',
		     'skill3_experience' => 'required|numeric',
		     'skill4' => 'required',
		     'skill4_experience' => 'required|numeric',
		     'skill5' => 'required',
		     'skill5_experience' => 'required|numeric',
		     'wfh_capability_1' => 'required',
		     'wfh_capability_2' => 'required',
		     'wfh_capability_3' => 'required',
		     'wfh_capability_4' => 'required',
		     'wfh_capability_5' => 'required'

		    ]); 


      	if ($validator->fails()) {
	            return response()->json(['error'=>$validator->errors()], 401);         

	        }

		 $fileName = null;
	   if (request()->hasFile('resume')) {
	        $file = request()->file('resume');
	        $fileName = md5($file->getClientOriginalName() . time()) . "." . $file->getClientOriginalExtension();
	        $file->move('./documents/', $fileName);    
	    }

	    // dd($file); mao ni ang testing
	   $applicationForm = new Applicant([
			'salutations'=> $request->get('salutations'),
			'firstName'=> $request->get('firstName'),
			'middleName'=> $request->get('middleName'),
			'lastName'=> $request->get('lastName'),
			'address'=> $request->get('address'),
			'email'=> $request->get('email'),
			'phone'=> $request->get('phone'),
			'position'=>$request->get('position'),
			'resume'=>  $fileName,
			'startdate'=>$request->get('startdate'),
			'skill1'=> $request->get('skill1'),
			'skill1_experience'=> $request->get('skill1_experience'),
			'skill2'=> $request->get('skill2'),
			'skill2_experience'=> $request->get('skill2_experience'),
			'skill3'=> $request->get('skill3'),
			'skill3_experience'=> $request->get('skill3_experience'),
			'skill4'=> $request->get('skill4'),
			'skill4_experience'=> $request->get('skill4_experience'),
			'skill5'=> $request->get('skill5'),
			'skill5_experience'=> $request->get('skill5_experience'),
		    'exp_1_position'=> $request->get('exp_1_position'),
	        'exp_1_company_and_year'=> $request->get('exp_1_company_and_year'),
	        'exp_1_descriptions'=> $request->get('exp_1_descriptions'),
	        'exp_2_position'=> $request->get('exp_2_position'),
	        'exp_2_company_and_year'=> $request->get('exp_2_company_and_year'),
	        'exp_2_descriptions'=> $request->get('exp_2_descriptions'),
	        'exp_3_position'=> $request->get('exp_3_position'),
	        'exp_3_company_and_year'=> $request->get('exp_3_company_and_year'),
	        'exp_3_descriptions'=> $request->get('exp_3_descriptions'),
	        'wfh_capability_1'=> $request->get('wfh_capability_1'),
	        'wfh_capability_2'=> $request->get('wfh_capability_2'),
	        'wfh_capability_3'=> $request->get('wfh_capability_3'),
	        'wfh_capability_4'=> $request->get('wfh_capability_4'),
	        'wfh_capability_5'=> $request->get('wfh_capability_5'),
	        'wfh_capability_6_reserve'=> $request->get('wfh_capability_6_reserve'),
	        'wfh_capability_7_reserve'=> $request->get('wfh_capability_7_reserve'),
	        'wfh_capability_8_reserve'=> $request->get('wfh_capability_8_reserve'),
	        'wfh_capability_9_reserve'=> $request->get('wfh_capability_9_reserve'),
	        'wfh_capability_10_reserve'=> $request->get('wfh_capability_10_reserve'),
		]);

		$applicationForm->save();

    	return response()->json($applicationForm);
    }

    public function getAllApplicants()
    {

    		// $getAllApplicants = Applicant::all();
    		$getAllApplicants = Applicant::orderBy('created_at', 'desc')->get();

    	return response()->json(array("data"=>$getAllApplicants));
    }
}
