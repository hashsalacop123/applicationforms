<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Applicant extends Model
{
    //
      protected $table="applicants";
      protected $fillable = [
     	'salutations',
     	'firstName',
     	'middleName',
     	'lastName',
     	'address',
     	'email',
     	'phone',
     	'position',
     	'resume',
     	'startdate',
     	'skill1',
     	'skill1_experience',
     	'skill2',
     	'skill2_experience',
     	'skill3',
     	'skill3_experience',
     	'skill4',
     	'skill4_experience',
     	'skill5',
     	'skill5_experience',
        'exp_1_position',
        'exp_1_company_and_year',
        'exp_1_descriptions',
        'exp_2_position',
        'exp_2_company_and_year',
        'exp_2_descriptions',
        'exp_3_position',
        'exp_3_company_and_year',
        'exp_3_descriptions',
        'wfh_capability_1',
        'wfh_capability_2',
        'wfh_capability_3',
        'wfh_capability_4',
        'wfh_capability_5',
        'wfh_capability_6_reserve',
        'wfh_capability_7_reserve',
        'wfh_capability_8_reserve',
        'wfh_capability_9_reserve',
        'wfh_capability_10_reserve'
    ];
}
