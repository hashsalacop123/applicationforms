<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDatacollectionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('dataCollections', function (Blueprint $table) {
                $table->id();
                $table->string('salutations',150)->nullable();
                $table->string('firstName',150);
                $table->string('middleName',150)->nullable();
                $table->string('lastName',150);
                $table->string('address',600);
                $table->string('skype',200);
                $table->string('email',200);
                $table->string('phone',150);
                $table->string('employment_status',150);
                $table->string('position',150);
                $table->string('startdate',150)->nullable();
                $table->string('salaray_range',150)->nullable();
                $table->string('resume');
                $table->string('skills');
                $table->string('personal_data_reserve1',150)->nullable();
                $table->string('personal_data_reserve2',150)->nullable();
                $table->string('personal_data_reserve3',150)->nullable();
                $table->string('personal_data_reserve4',150)->nullable();
                $table->string('personal_data_reserve5',150)->nullable();
                $table->string('wfh_capability_1',150)->nullable();
                $table->string('wfh_capability_2',150)->nullable();
                $table->string('wfh_capability_3',150)->nullable();
                $table->string('wfh_capability_4',150)->nullable();
                $table->string('wfh_capability_5',150)->nullable();
                $table->string('wfh_capability_6',150)->nullable();
                $table->string('wfh_capability_7',150)->nullable();
                $table->string('wfh_capability_8',150)->nullable();
                $table->string('wfh_capability_9',150)->nullable();
                $table->string('wfh_capability_10',150)->nullable();
                $table->string('wfh_capability_11',150)->nullable();
                $table->string('wfh_capability_12',150)->nullable();
                $table->string('wfh_capability_13',150)->nullable();
                $table->string('wfh_capability_14',150)->nullable();
                $table->string('wfh_capability_15',150)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('datacollections');
    }
}
