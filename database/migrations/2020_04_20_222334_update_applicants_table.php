<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateApplicantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
         Schema::table('applicants', function (Blueprint $table) {;
              $table->string('exp_1_position',150)->nullable();
              $table->string('exp_1_company_and_year',400)->nullable();
              $table->string('exp_1_descriptions')->nullable();
              $table->string('exp_2_position',150)->nullable();
              $table->string('exp_2_company_and_year',400)->nullable();
              $table->string('exp_2_descriptions')->nullable();
              $table->string('exp_3_position',150)->nullable();
              $table->string('exp_3_company_and_year',400)->nullable();
              $table->string('exp_3_descriptions')->nullable();
              $table->string('wfh_capability_1',150);
              $table->string('wfh_capability_2',150);
              $table->string('wfh_capability_3',150);
              $table->string('wfh_capability_4',150);
              $table->string('wfh_capability_5',150);
              $table->string('wfh_capability_6_reserve')->nullable();
              $table->string('wfh_capability_7_reserve')->nullable();
              $table->string('wfh_capability_8_reserve')->nullable();
              $table->string('wfh_capability_9_reserve')->nullable();
              $table->string('wfh_capability_10_reserve')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
