<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateApplicantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('applicants', function (Blueprint $table) {
            $table->id();
            $table->string('salutations',150)->nullable();
            $table->string('firstName',150);
            $table->string('middleName',150)->nullable();
            $table->string('lastName',150);
            $table->string('address',600);
            $table->string('email',200);
            $table->string('phone',150);
            $table->string('position',150);
            $table->string('resume');
            $table->string('startdate',150)->nullable();
            $table->string('skill1');
            $table->string('skill1_experience');
            $table->string('skill2');
            $table->string('skill2_experience');
            $table->string('skill3');
            $table->string('skill3_experience');
            $table->string('skill4');
            $table->string('skill4_experience');
            $table->string('skill5');
            $table->string('skill5_experience');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('applicants');
    }
}
