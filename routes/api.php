<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::post('login', 'API\UserController@login');
Route::post('register', 'API\UserController@register');
// Route::post('application', 'API\applicantsController@applicationForm');
Route::post('datacollections','API\DataCollectionsController@datacollections');

Route::group(['middleware' => 'auth:api'], function(){

	Route::get('applicants','API\applicantsController@getAllApplicants');
	Route::get('applicantsquerys','API\DataCollectionsController@dataQueryadmin');
	Route::get('skillsquery','API\DataCollectionsController@dataSkillsQUery');
	Route::get('mainskills','API\DataCollectionsController@mainSkills');
	Route::post('breakskills/skills','API\DataCollectionsController@mainSkillsBreakDown');
	Route::post('positionquery','API\DataCollectionsController@postionQueryData');
	Route::get('summary/{id}','API\DataCollectionsController@queryApplicantsBy');
	Route::get('alldataapplicants','API\DataCollectionsController@allapplication');
	///testing mike 123456666
});

